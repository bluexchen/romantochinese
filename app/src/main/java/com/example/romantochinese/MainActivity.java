package com.example.romantochinese;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView chineseNumText;
    EditText romanNumEdit;
    Button convertBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        chineseNumText = findViewById(R.id.text_chinese_num);
        romanNumEdit = findViewById(R.id.input_roman_num);
        convertBtn = findViewById(R.id.btn_convert);
        convertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chineseNumText.setText(NumberUtil.romanToChinese(romanNumEdit.getText().toString().trim()));
            }
        });


    }
}
