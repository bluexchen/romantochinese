package com.example.romantochinese;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class NumberUtil {

    public static String romanToChinese(String input) {

        String result = "";
        try {
            result = calIntToChinese(romanToArabic(input));
        }catch (IllegalArgumentException e) {
            return "Cannot recognize this Roman Number!";
        }

        return result;
    }

    //將阿拉伯數字轉成中文數字
    private static String calIntToChinese(int input) {

        //目前沒有處理萬以上的數字
        if(input > 9999) {
            return "Cannot recognize this Roman Number!";
        }

        String chineseNum = "";

        //將數字切成digits
        int i = input;
        List<Integer> digits = new ArrayList<Integer>();
        while(i > 0) {
            digits.add(i % 10);
            i /= 10;
        }

        //處理數字後面為0的狀況，替換成-1
        for(int j=0; j< digits.size(); j++) {
            if(digits.get(j) == 0) {
                digits.set(j, -1);
            }else {
                break;
            }
        }

        int digitSize = digits.size();


        if(digitSize == 0) {
            return "零";
        }

        //開始將digit轉換拼成中文數字
        int x = digitSize - 1;
        while(x >= 0 && digits.get(x) != -1){
            System.out.println("Position:" + x + ", value = " + digits.get(x));
            chineseNum += numPosToChinese(digits.get(x), x);
            x--;
        }

        return chineseNum;

    }

    //將羅馬數字轉成阿拉伯數字
    private static int romanToArabic(String input) {
        String romanNumeral = input.toUpperCase();
        int result = 0;

        List<RomanNumeral> romanNumerals = RomanNumeral.getReverseSortedValues();

        int i = 0;

        //利用Enum把羅馬parse並加總它們的值
        while ((romanNumeral.length() > 0) && (i < romanNumerals.size())) {
            RomanNumeral symbol = romanNumerals.get(i);
            if (romanNumeral.startsWith(symbol.name())) {
                result += symbol.getValue();
                romanNumeral = romanNumeral.substring(symbol.name().length());
            } else {
                i++;
            }
        }

        if (romanNumeral.length() > 0) {
            throw new IllegalArgumentException(input + " cannot be converted to a Roman Numeral");
        }

        return result;
    }

    //把阿拉伯數字字元位置轉換成 千、百、十
    private static String numPosToChinese(int num, int position) {
        if(num == 0) {
            return ArabicToChinese(num);
        }
        switch(position) {
            case 0:
                return ArabicToChinese(num);
            case 1:
                return (ArabicToChinese(num) + "十");
            case 2:
                return (ArabicToChinese(num) + "百");
            case 3:
                return (ArabicToChinese(num) + "千");
            default:
                return "";
        }
    }

    //把0~9數字轉換成國字，-1是位於數字後面的0，則不顯示國字
    private static String ArabicToChinese(int i) {
        String c = "";
        switch(i) {
            case -1:
                c="";
                break;
            case 0:
                c="零";
                break;
            case 1:
                c="一";
                break;
            case 2:
                c="二";
                break;
            case 3:
                c="三";
                break;
            case 4:
                c="四";
                break;
            case 5:
                c="五";
                break;
            case 6:
                c="六";
                break;
            case 7:
                c="七";
                break;
            case 8:
                c="八";
                break;
            case 9:
                c="九";
                break;
        }
        return c;
    }


}


enum RomanNumeral {
    I(1), IV(4), V(5), IX(9), X(10),
    XL(40), L(50), XC(90), C(100),
    CD(400), D(500), CM(900), M(1000);

    private int value;

    RomanNumeral(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static List<RomanNumeral> getReverseSortedValues() {
        return Arrays.stream(values())
                .sorted(Comparator.comparing((RomanNumeral e) -> e.value).reversed())
                .collect(Collectors.toList());
    }
}



